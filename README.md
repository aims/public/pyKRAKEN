# lvKRAKEN LabView library

KRAKEN Represents Additional Knowledge (about) Experiment Numbers

Python library and utility to create interlinked context entities describing a measuring system, quantities, observations, datasets etc. based on rdf and rdf based vocabularies, specifically the SOSA ontology.

## Dependencies

* rdflib
* uuid6
* h5py

## Installation

Add the pyKRAKEN directory to your source code repository.

## Documentation

[go to wiki](../../wikis/home)

## License

This repository is licensed under the EUPL-1.2 or later. See the [LICENSE deed](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) for details.
